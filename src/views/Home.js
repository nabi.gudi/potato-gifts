import React, {Component} from 'react';
import {Switch, Route} from 'react-router-dom';
import '../styles/Home.scss';
import Header from '../components/Header';
import ProductList from '../components/ProductList';
import Profile from '../components/Profile';
import History from '../components/History';
import Default from '../components/Default';
import Footer from '../components/Footer';

class Home extends Component {  
  constructor(props) {
    super(props);
    this.state = { 
      points: 0,
      add: false,
      substract: false,
      productId: ""
    };

    this.add = this.add.bind(this);
    this.substract = this.substract.bind(this);
  }  
  add(points=1) {
    console.log(points);
    var add = true;
    var substract = false;
    this.setState({add}); 
    this.setState({substract}); 
    this.setState(() => ({
      points: this.state.points + parseInt(points),
    }));    
  };
  substract(productId, points) {
    var add = false;
    var substract = true;
    this.setState({add}); 
    this.setState({substract}); 
    this.setState({productId}); 
    this.setState(() => ({
      points: this.state.points + parseInt(points),
    }));    
  };

  render() {
    const points = this.state.points,
    productId  = this.state.productId,
    addPoints = this.state.add,
    substractPoints = this.state.substract;
    return (  
      <div className="Home">
        <React.Fragment>
          <Header points={points} add={addPoints} substract={substractPoints} productId={productId}></Header>
          <div className="main-container">
          <Switch>
            <Route exact path="/" 
                  component={props => <ProductList substract={(productId, points) => this.substract(productId, points)}  />}></Route>
            <Route exact path="/profile/:id" 
                  component={props => <Profile add={(points) => this.add(points)}  />}></Route>
            <Route exact path="/history/" component={props => <History/>}></Route>          
            <Route component={Default}></Route>
          </Switch>
          </div>
          
          <Footer></Footer>
        </React.Fragment>
      </div>
    )
  }
}

export default Home;
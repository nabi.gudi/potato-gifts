import React, {Component} from 'react';
import axios from 'axios';
import '../styles/Product.scss';
import potapointsN from '../assets/Potapoints-N.png';
import classNames  from 'classnames';

export default class Product extends Component {
  state = {
    user: []
  }

  render() {
    var product = {
      id: this.props.id,
      img: this.props.img,
      title: this.props.title,
      description: this.props.description,
      points: this.props.points
    };
    
    const headersDef = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1Y2UwMzJkZjU4YzIzNzAwNmQxMDk0NTAiLCJpYXQiOjE1NTgxOTY5NTl9.NpMYPCIxqWmYhlQLBv3dwMDV16tSuzMnAV6WYxX07LE'
    }
    axios.get('https://coding-challenge-api.aerolab.co/user/me', { headers: headersDef})
      .then(res => {
        const user = res.data;
        this.setState({ user });
      });

    let user = this.state.user;
    let button;
    let pointsLeft = product.points - user.points;

    var isAvailable = classNames( this.props.className, {
      'available': ( user.points >= product.points ),
      'product': true
    });

    if (user.points >= product.points) {
      button = <div className="product-hover-redeem_button"
           onClick={(productId, points) => this.props.substract(product.id, product.points)}
      >Redeem now!</div>
    } else {
      button = 
      <React.Fragment>
        <p className="product-hover-redeem_text">You only need {pointsLeft} </p>
        <p className="product-hover-redeem_text">potapoints more!</p>
      </React.Fragment>
    }

    return(
      <div className={isAvailable}>
        <div className="product-container">
          <div className="product-container-image_container">
            <div className="product-container-image_container-loading">
              <div className="lds-ripple">
                <div></div>
                <div></div>
              </div>
            </div>
            <img className="product-container-image_container-img" src={product.img} alt="product"/>
          </div>
          <div className="product-container-details">
            <label className="product-container-details_title">{product.title}</label>
            <p className="product-container-details_description">{product.description}</p>
            <div className="product-container-details-points">
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" className="product-container-details-points_potapoints"><g transform="translate(-24 -565.5)"><text transform="translate(28 584.5)" className="product-container-details-points_potapoints_color" fill="#f22738" fontSize="20" fontFamily="Raleway" fontWeight="bold"><tspan x="0" y="0">P</tspan></text><text transform="translate(31 584.5)" className="product-container-details-points_potapoints_color" fill="#f22738" fontSize="20" fontFamily="Raleway" fontWeight="bold"><tspan x="0" y="0">P</tspan></text><g transform="translate(24 565.5)" fill="rgba(255,255,255,0)"><path d="M 12 23.5 C 8.928239822387695 23.5 6.040329933166504 22.30378913879395 3.868269920349121 20.13172912597656 C 1.696210026741028 17.95966911315918 0.5 15.0717601776123 0.5 12 C 0.5 8.928239822387695 1.696210026741028 6.040329933166504 3.868269920349121 3.868269920349121 C 6.040329933166504 1.696210026741028 8.928239822387695 0.5 12 0.5 C 15.0717601776123 0.5 17.95966911315918 1.696210026741028 20.13172912597656 3.868269920349121 C 22.30378913879395 6.040329933166504 23.5 8.928239822387695 23.5 12 C 23.5 15.0717601776123 22.30378913879395 17.95966911315918 20.13172912597656 20.13172912597656 C 17.95966911315918 22.30378913879395 15.0717601776123 23.5 12 23.5 Z" stroke="none"/><path className="product-container-details-points_potapoints_color" d="M 12 1 C 9.061790466308594 1 6.299449920654297 2.144199371337891 4.221820831298828 4.221820831298828 C 2.144199371337891 6.299449920654297 1 9.061790466308594 1 12 C 1 14.93820953369141 2.144199371337891 17.7005500793457 4.221820831298828 19.77817916870117 C 6.299449920654297 21.85580062866211 9.061790466308594 23 12 23 C 14.93820953369141 23 17.7005500793457 21.85580062866211 19.77817916870117 19.77817916870117 C 21.85580062866211 17.7005500793457 23 14.93820953369141 23 12 C 23 9.061790466308594 21.85580062866211 6.299449920654297 19.77817916870117 4.221820831298828 C 17.7005500793457 2.144199371337891 14.93820953369141 1 12 1 M 12 0 C 18.62742042541504 0 24 5.372579574584961 24 12 C 24 18.62742042541504 18.62742042541504 24 12 24 C 5.372579574584961 24 0 18.62742042541504 0 12 C 0 5.372579574584961 5.372579574584961 0 12 0 Z" stroke="none" fill="#f22738"/></g></g></svg>
              <label className="product-container-details-points_number">{product.points}</label>
            </div>
          </div> 
        </div>
        <div className="product-background"></div>
        <div className="product-hover">
          <div className="product-hover-points">
            <img className="product-hover-points_potapoints" src={potapointsN} alt="potapoints"/>
            <label className="product-hover-points_number">{product.points}</label>
          </div>
          <div className="product-hover-redeem">
            {button}     
          </div>
        </div>
      </div>
    )
  }
}
import React, {Component} from 'react';
import axios from 'axios';
import { NavLink } from 'react-router-dom'
import userPhoto from '../assets/user.png';
import potapoints from '../assets/Potapoints.png';
import '../styles/User.scss';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default class User extends Component {  
  state = {
    user: [],
    points: 0,
    productId: ""
  }

  addUserPoints(){    
    const headersDef = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1Y2UwMzJkZjU4YzIzNzAwNmQxMDk0NTAiLCJpYXQiOjE1NTgxOTY5NTl9.NpMYPCIxqWmYhlQLBv3dwMDV16tSuzMnAV6WYxX07LE'
    }
    axios.post('https://coding-challenge-api.aerolab.co/user/points', 
      {amount : this.state.points} ,
      {headers: headersDef})
      .then(res => {
        const message = res.data.message;
        this.setState({ message });
        this.notify(message);
      });
  }

  redeemPoints(){
    const headersDef = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1Y2UwMzJkZjU4YzIzNzAwNmQxMDk0NTAiLCJpYXQiOjE1NTgxOTY5NTl9.NpMYPCIxqWmYhlQLBv3dwMDV16tSuzMnAV6WYxX07LE'
    }
    axios.post('https://coding-challenge-api.aerolab.co/redeem', 
     {productId: this.state.productId},
     { headers: headersDef})
      .then(res => {
        let message = res.data.message;
        this.setState({ message });
        this.notify(message);
      });
  
  }

  componentDidMount() {
    const headersDef = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1Y2UwMzJkZjU4YzIzNzAwNmQxMDk0NTAiLCJpYXQiOjE1NTgxOTY5NTl9.NpMYPCIxqWmYhlQLBv3dwMDV16tSuzMnAV6WYxX07LE'
    }
    axios.get('https://coding-challenge-api.aerolab.co/user/me', { headers: headersDef})
      .then(res => {
        const user = res.data;
        this.setState({ user });
        console.log(user);
      });
  }

  addPoints(points){
    let user = this.state.user;
    user.points = user.points + points;
    this.setState({user});
    this.state.points = points;
    this.addUserPoints();
  }

  substractPoints(productId, points){
    this.state.productId = productId;
    let user = this.state.user;
    user.points = user.points - points;
    this.setState({user});
    this.state.points = points;
    this.redeemPoints();
  }

  check(){
    const {add, substract, points, productId} = this.props;
    if (add){
      this.addPoints(points);
    } else{
      if(substract){
        this.substractPoints(productId, points);
      }
    }
  }


  notify (message){
    toast.success(message, {
      position: toast.POSITION.TOP_RIGHT
    });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.points !== this.props.points || prevProps.productId !== this.props.productId) {
      this.check();
    }
  }

  render() {
    let user = this.state.user;

    return (
      <React.Fragment>
      <ToastContainer />
      <NavLink to={`/profile/${user._id}`} className="user_link">
        <div className="user">
          <img src={userPhoto} className="user-img" alt="user"></img>
          <label className="user-name">{user.name}</label>
          <div className="user-points">
            <img src={potapoints} className="user-points_logo" alt="potapoints"></img>
            <label className="user-points_quantity">{user.points}</label>
          </div>
        </div>
      </NavLink>      
      </React.Fragment>
    )
  }
}
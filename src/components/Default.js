import React, {Component} from 'react';
import '../styles/Default.scss';

export default class Default extends Component {
  render() {
    return (
      <div className="default">        
        <label className="default-title">Ups... Nobody here :(</label>

        <label className="default-subtitle">Please check your url.</label>
      </div>
    )
  }
}
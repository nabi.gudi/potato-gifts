import React, {Component} from 'react';
import axios from 'axios';
import '../styles/ProductList.scss';
import Paginator from '../components/Paginator';
import Product from '../components/Product';

export default class ProductList extends Component {
  state = {
    products: [], 
    actualProductsPage: [],
    totalCount: 0,
    totalPages: 1,
    currentPage: 1,
    pageSize: 10,
    firstProduct: 0,
  };     

  productList = []; 

  componentDidMount() {
    const headersDef = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1Y2UwMzJkZjU4YzIzNzAwNmQxMDk0NTAiLCJpYXQiOjE1NTgxOTY5NTl9.NpMYPCIxqWmYhlQLBv3dwMDV16tSuzMnAV6WYxX07LE'
      }
    axios.get('https://coding-challenge-api.aerolab.co/products', { headers: headersDef})
      .then(res => {
        const products = res.data,
          totalCount = res.data.length,
          totalPages = Math.ceil(totalCount/ this.state.pageSize),
          actualProductsPage = res.data.slice(this.state.firstProduct, this.state.pageSize);
        this.setState({ products, totalCount, totalPages, actualProductsPage});
      })
  }

  sortByLowest(){
    this.setState({
      products: this.state.products.sort((a, b) =>  a.cost - b.cost ),
      firstProduct: 0,
      currentPage: 1});
  }

  sortByHighest(){
    this.setState({
      products: this.state.products.sort((a, b) =>  b.cost - a.cost ),
      firstProduct: 0,
      currentPage: 1});
  }

  sortBy(method){  
    if (method === "lowest"){
      this.sortByLowest();
    } else{
      if (method === "highest"){
      this.sortByHighest();
      } 
    }
    this.setState({ 
      actualProductsPage: this.state.products.slice(0, 10)
    });
  }

  nextPage (){
    if (this.state.totalPages >= this.state.currentPage){ 
      const fp = this.state.firstProduct+this.state.pageSize,
      cp = this.state.currentPage+1;
      this.setState({ 
        firstProduct: fp,
        currentPage: cp,
        actualProductsPage: this.state.products.slice(fp, cp*this.state.pageSize)
      });
    }    
  }

  prevPage(){
    if (this.state.currentPage>1){ 
      const fp = this.state.firstProduct-this.state.pageSize,
      cp = this.state.currentPage-1;
      this.setState({ 
        firstProduct: fp,
        currentPage: cp,
        actualProductsPage: this.state.products.slice(fp, cp*this.state.pageSize)
      });
    }  
  }

  render() {    
    const {substract} = this.props;    
    this.productList = this.state.actualProductsPage.map((products) =>
    <Product 
       key={products._id} 
       id={products._id} 
       img={products.img.url} 
       title={products.name} 
       description={products.category} 
       points={products.cost}
       substract= {(productId, points) => substract(products._id, products.cost)}
    ></Product>
    );

    return (       
      <React.Fragment>
        <Paginator 
          totalCount={this.state.totalCount}
          actualPage={this.state.currentPage}
          totalPage={this.state.totalPages} 
          filter={true} 
          clickHandler={ sortType => this.sortBy(sortType)}
          prevHandler={ () => this.prevPage()}
          nextHandler={ () => this.nextPage()}/>
        <div className="product-list_list">
          {this.productList}
        </div>
        <Paginator 
          totalCount={this.state.totalCount}
          actualPage={this.state.currentPage}
          totalPage={this.state.totalPages} 
          filter={false} 
          clickHandler={ sortType => this.sortBy(sortType)}
          prevHandler={ () => this.prevPage()}
          nextHandler={ () => this.nextPage()}/>
      </React.Fragment>
    )
  }
}
import React, {Component} from 'react';
import '../styles/Paginator.scss';
import FilterList from '../components/FilterList';
import arrowLeft from '../assets/arrow-left.svg';
import arrowRight from '../assets/arrow-right.svg';

export default class Paginator extends Component {
  render() {
    const {totalCount, actualPage, totalPage, filter, clickHandler, prevHandler, nextHandler} = this.props;

    return (
      <div className="paginator">        
        <div className="paginator-left_details">          
          { filter &&
            <FilterList clickHandler={sortType => clickHandler(sortType)}></FilterList>
          }
        </div>
        <div className="paginator-right_details">
          <label className="paginator-right_details_text">{totalCount} gifts • Page {actualPage} of {totalPage}</label> 
          <img className="paginator-right_details-arrow_left" src={arrowLeft} alt="arrow-left" onClick={prevHandler}/>
          <img className="paginator-right_details-arrow_right" src={arrowRight} alt="arrow-right" onClick={nextHandler}/>
        </div>
      </div>
    )
  }
}
import React, {Component} from 'react';
import { NavLink } from 'react-router-dom'
import logo from '../assets/potato.png';
import User from '../components/User';
import '../styles/Header.scss';

export default class Header extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      points: 0,
      add: false,
      substract: false,
      productId: ""
    };

    this.add = this.add.bind(this);
    this.substract = this.substract.bind(this);
  }  

  add(points) {
    var add = true;
    var substract = false;
    this.setState({add}); 
    this.setState({substract}); 
    this.setState({points});    
  };
  substract(productId, points) {
    var add = false;
    var substract = true;
    this.setState({add}); 
    this.setState({substract}); 
    this.setState({points});    
    this.setState({productId});    
  };

  render() {
    const { points, add, substract, productId } = this.props;
    return (
      <header className="header">  
        <NavLink className="header-logo" to={"/"} >
          <img src={logo} className="header-logo_img" alt="logo"></img>
          <label className="header-logo_text">Potato Gifts</label>
        </NavLink>
        <User points={points} add={add} substract={substract} productId={productId}></User>          
      </header>
    )
  }
}
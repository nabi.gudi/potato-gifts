import React, {Component} from 'react';
import '../styles/Filter.scss';

export default class Filter extends Component {
  render() {
    const { clickHandler, sort, option } = this.props;
  
    return (
      <button 
        className="filter"
        key={sort} 
        data-sort={sort}
        onClick={() => clickHandler(sort)}>        
          {option}
      </button>
    )
  }
}
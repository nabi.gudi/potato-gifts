import React, {Component} from 'react';
import '../styles/FilterList.scss';
import Filter from '../components/Filter';

export default class FilterList extends Component {
  render() {
    const filters = [
        {
          id: 1,
          option: "Lowest price",
          sort: "lowest"
        }, 
        {
          id: 2,
          option: "Highest price",
          sort: "highest"
        }
    ];
    const {clickHandler} = this.props;

    const filterList = filters.map((filter) => 
      <Filter 
        sortType = {filter.sort}
        key={filter.id}
        option={filter.option}
        sort={filter.sort}
        clickHandler={sortType => clickHandler(sortType)}>
      </Filter>
    );  

    return (        
      <React.Fragment>
        <div className="filter-list_list">
            Sort by: {filterList}
        </div>
      </React.Fragment>
    )
  }
}
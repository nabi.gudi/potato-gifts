# Potato Gifts

![Potato Gifts Logo](/screenshots/Logo.png)

## The project
Potato Gifts is a redeem products website. Here, the users can transform their potapoints (points) into awesome products. Potato Gifts is an MVP where you are the user John Kite. He can redeem any product on the site if he has enough points. This user also has some development tools, like adding points to his own. 
**To see the project running, go to: [https://potato-gifts.nabigudi.now.sh/](https://potato-gifts.nabigudi.now.sh/)**

## The skeleton
The tooling and criteria used for this project was:
* Create React App
* Axios
* Responsive Web Design
* Mobile First
* Zeit Now

## How to run the app?
`npm start` runs the app in development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser. 

## How it works?
Well, when you go into this website, you will find something like this:<br>
![Main section](/screenshots/main.png)
<br><br>
Your potapoints are on the **upper right corner** with your name and avatar:<br>
![WhoAreYou section](/screenshots/whoAreYou.png)
<br><br>
Wherever you are, you can came back to the main section with a click on the logo on the **upper left corner**:<br>
![potatoGifts section](/screenshots/potatoGifts.png)
<br><br>
You *can redeem* the products which are **pink lighted** like this:<br>
![enabled product](/screenshots/enabled.png)
<br><br>
Also if you hover them, you will see the **redeem button**:<br>
![redeemNow product](/screenshots/redeemNow.png)
<br><br>
You *can't redeem* the products which are **gray lighted** like this:<br>
![disabled product](/screenshots/disabled.png)
<br><br>
You can see **how many extra potapoints** you need if you hover them:<br>
![needExtraPoints product](/screenshots/needExtraPoints.png)
<br><br>
Do you want **more products**? Check the other pages...<br>
![moreResults section](/screenshots/moreResults.png)
<br><br>
Do you want to **sort** them? No prob, click on the sorting method you want...<br>
![sortResults section](/screenshots/sortResults.png)
<br><br>
**Have a nice redeem! :)**

## Next steps
* **Add login view:** we can receive multiple users
* **Add description to all the products:** now the products only have name and category. It's very useful to give the users some extra information about each product.
    * **Add product detail view:** we need to add a detail view in every product so the user can inspect each product detail before redeem it.
    * **Move the "Redeem now!" button:** we need to make sure the users don't redeem by mistake any product when they're just trying to go to the product detail view.
* **All profile actions:** users can edit their profile (add a photo, change username, etc.).
* **Redeem cart:** users can choose several products to redeem and then check and redeem them in only one click.